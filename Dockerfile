# SPDX-FileCopyrightText: 2023 Steffen Vogel <post@steffenvogel.de>
# SPDX-License-Identifier: CC0-1.0

FROM debian:bookworm-slim

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
        clang pkg-config nettle-dev libpcsclite-dev pcsc-tools curl socat wget ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain stable -y

ARG OPENPGP_CARD_TOOLS_VERSION="0.9.5"

RUN . "$HOME/.cargo/env" \
 && cargo install openpgp-card-tools --version $OPENPGP_CARD_TOOLS_VERSION
