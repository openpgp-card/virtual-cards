# SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
# SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
# SPDX-License-Identifier: CC0-1.0

FROM debian:bookworm-slim AS card

ARG REPO="https://github.com/canokeys/canokey-core.git"
ARG VERSION="2.0.0"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
        git pkg-config cmake make build-essential libcmocka-dev libpcsclite-dev pcscd pcsc-tools ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN git clone --depth 1 --branch $VERSION $REPO

RUN cd canokey-core \
    && git submodule update --init --recursive

RUN cd canokey-core \
    && cmake -S . -B build \
        -DENABLE_TESTS=ON \
        -DVIRTCARD=ON \
        -DENABLE_DEBUG_OUTPUT=ON \
        -DCMAKE_BUILD_TYPE=Debug \
    && cmake --build build --verbose --parallel

RUN cp canokey-core/build/libu2f-virt-card.so /usr/local/lib/ \
    && cp canokey-core/test-via-pcsc/pcscd-reader.conf /etc/reader.conf.d/

COPY start.sh /

FROM card AS builddeps

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
        curl clang pkg-config nettle-dev \
    && rm -rf /var/lib/apt/lists/*

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain stable -y

ENV PATH="$HOME/.cargo/bin:$PATH"

FROM card
