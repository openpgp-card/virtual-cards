#!/bin/env bash
# SPDX-FileCopyrightText: 2023 Steffen Vogel <post@steffenvogel.de>
# SPDX-License-Identifier: CC0-1.0

export VSMARTCARD_HOST=pcscd

if [ -f jcardsim.cfg ]; then
    sed -i \
        -e "/^com.licel.jcardsim.vsmartcard.host/s/=.*$/=${VSMARTCARD_HOST:-vsmartcard}/" \
        -e "/^com.licel.jcardsim.vsmartcard.port/s/=.*$/=${VSMARTCARD_PORT:-35963}/" \
        jcardsim.cfg
fi

sh run-card.sh

while true; do sleep 3600; done
