# SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

echo "Starting jcardsim" 1>&2
java -cp ./jcardsim/target/jcardsim-3.0.5-SNAPSHOT.jar:class/ com.licel.jcardsim.remote.VSmartCard jcardsim.cfg &
echo "Started jcardsim" 1>&2

sleep 10

echo "Running opensc-tool" 1>&2
opensc-tool --card-driver default --send-apdu 80b800002210D276000124010200fff100000001000010D276000124010200fff100000001000000 1>&2
echo "Done running opensc-tool" 1>&2
