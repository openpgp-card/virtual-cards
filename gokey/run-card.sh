# SPDX-FileCopyrightText: 2023 Steffen Vogel <post@steffenvogel.de>
# SPDX-License-Identifier: CC0-1.0

/gokey_vpcd -c ${VSMARTCARD_HOST-127.0.0.1}:${VSMARTCARD_PORT-35963} &

sleep 2

echo "Started gokey_vpcd" 1>&2
