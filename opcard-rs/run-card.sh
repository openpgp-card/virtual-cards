# SPDX-FileCopyrightText: 2022 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

/opcard-rs-virtual &

sleep 2

echo "Started opcard-rs virtual" 1>&2
