# SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

FROM debian:bookworm-slim AS card

ARG REPO="https://github.com/frankmorgner/vsmartcard.git"
ARG VERSION="virtualsmartcard-0.9"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
         autoconf libtool m4 automake pkg-config gettext help2man git make python3-dev python3-setuptools python3-pip libpcsclite-dev pcscd pcsc-tools opensc \
    && rm -rf /var/lib/apt/lists/*

RUN git clone --depth 1 --branch $VERSION $REPO \
    && cd vsmartcard/virtualsmartcard \
    && autoreconf -vis && ./configure --enable-vpcdslots=15 \
    && make install clean

# Note: We build vsmartcard with support for up to 15 slots
# Strangely 16 slots are broken eventhough it should be supported by
# pcsc-lite
# See: https://github.com/frankmorgner/vsmartcard/issues/155
