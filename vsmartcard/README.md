<!--
SPDX-FileCopyrightText: 2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: CC-BY-4.0
-->

Container image of http://frankmorgner.github.io/vsmartcard/virtualsmartcard/README.html
